import './scss/index.scss';

import { dcFactory } from '@deleteagency/dc';
import HomePageComponent from './ts/home-page.component';

dcFactory.register(HomePageComponent);
