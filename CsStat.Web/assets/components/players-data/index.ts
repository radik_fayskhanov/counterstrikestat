import './scss/index.scss';

import { dcFactory } from '@deleteagency/dc';
import PlayersDataComponent from './ts/players-data.component';

dcFactory.register(PlayersDataComponent);
